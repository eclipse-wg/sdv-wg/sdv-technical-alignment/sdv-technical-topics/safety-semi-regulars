# Safety Semi-Regulars

This is a little exploratory project to investigate possibilities for formalizing and automating work with safety software elements out of context (SEooC, SOUP, whichever standard's wording you want to apply), in an upstream-downstream scenario.

Currently this discussion is very explorative and open. It might lead to a hands-on project down the line, some inputs that are offered to other (standardization) groups, something else entirely, or nothing at all.

## Related SDV projects

- none known

## Use cases

Think of something like this, but in an upstream-downstream scenario:

- [Deterministic Construction Service (codethink.co.uk)](https://www.codethink.co.uk/articles/2021/deterministic-construction-service/)
- [STPA for Software Intensive Systems (codethink.co.uk)](https://www.codethink.co.uk/articles/2021/stpa-software-intensive-systems/)

## Cross Initiatives Collaboration

Strong topical links to [ELISA OSEP (github.com)](https://github.com/elisa-tech/wg-osep)
Further inspiration: [Developing Complex Safety Critical Systems in Complex Supply Chains
 (ieeexplore.ieee.org)](https://ieeexplore.ieee.org/document/9236958)