# Meeting 27th February 2023

## Participants

- Daniel Krippner (ETAS)
- Robert Fekete (Volvo Cars)
- Johannes Baumgartl (Mercedes)
- Christopher Temple (ARM)

## Notes

- Introductions of  new participants
- Going into discussion of the attempt to describe "AoU"-type requirements for solution elements
- ... agreeing that starting with an "eGas"-style bounded scope will be likely best path
