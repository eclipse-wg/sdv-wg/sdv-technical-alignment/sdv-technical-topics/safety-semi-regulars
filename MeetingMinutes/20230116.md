# Meeting 16th January 2023

## Participants

- Paul Albertella (Codethink)
- Daniel Krippner (ETAS)
- Markus Schurius (BMW)

## Notes

- The group would move this activity to the Eclipse SDV project
  - a GitLab wiki space there
  - group members to check wether they are ok with Eclipse contribution agreement/bylaws
- Markus highlighted that there is a definitive issue with "AoU juggling" in complex projects (examples below)
  - can we explicitly call out this problem, create awareness
  - can we establish an accepted way to make this easier for people
  - is the gap in the standards that AoUs are not formulated in a modular way
  - ... and thus are not clearly verifiable in system context
  - part of solution: along with AoUs, provide clear list of AoU verification aspects/goals/needs
  - STPA approach can support this kind of scenario
  - Standard requires that AoUs are defined, but is very loose about details
- Where to start?
  - describe problem
  - define example approaches to manage the problem
  - "How do you show that you met the intent of the AoU in your system context?"
- Next steps:
  - Markus to provide 1-2 examples for "problematic" AoUs, and how they might be improved
  - us (Daniel...) to begin a problem statement write-up, for raising awareness/contributing to ISO, etc

---

## Example Scenarios

- conflict between goals to be fail-safe vs fail-operational
  - this kind of information would be helpful as AoU context

### Need to control access to a shared resource

Software is so configurable/complex (and uses code generation, etc) that it is hard for software provider to point to specific system resources that should not be interfered with.
The AoU is "simple" - but the actual details of implementation/usage patterns etc in a system are completely obscure.

In a supply chain (2 parties), every party can only do half of the job. Both parties would need know-how on the other's half to be accurate about their part.

### Need to perform substantiated system (e.g. performance) profiling

In a component-based system, one part of the supply chain can only know what "realistic" or "worst-case" scenarios look like for the functionality in their scope - how could a system-wide realistic scenario be found?
