# Meeting 6th March 2023

## Participants

- Daniel Krippner (ETAS)
- Johannes Baumgartl (Mercedes)
- Christopher Temple (ARM)
- Markus Schurius (BMW)

## Notes

- How can we come to grips with the topic, how to generate results?
  - conflict between
    - EooCs want to be generic in their AoU definitions as possible, to be broadly consumable
    - system integrators/designers need AoUs to be as specific as possible to their system context
    - the safe approach would be: only begin with project/design once all AoUs are available and you did all the risk/gap analysis
      -> but not feasible in reality
  - many AoUs aren't really AoUs because there's no safety-claim they are linking to/implying

- Idea: can we propose 'Best practices' for writing AoUs? e.g.
  - require safe/deterministic fail-safe design descriptions, instead of "function must not fail"

Examples areas, -patterns:
 - OS-style MoUs in separate document: MoUexamples.md
 - define the safety aspects interaction between external power supply any internal pwr supply management
 - other domain, electricity: even though the power plant is taking safety measures (breakers) on their side, still breakers are required in house, apartment, room, appliance levels (even though plant-level surely is certified to whatever standard)
-> this might become an analogy for a HW/SW system MoU best practice
