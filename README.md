# Safety Semi-Regulars

This is a little exploratory project to investigate possibilities for formalizing and automating work with safety software elements out of context (SEooC, SOUP, whichever standard's wording you want to apply), in an upstream-downstream scenario.

## The Idea

Put together a fully featured software system that comprises example upstream "SEooC/SOUP" component(s) and provides a blueprint for a software integration project that consumes that upstream (Open Source) IP. This blueprint should put a focus on showing the interaction and role-split between what an upstream code base can deliver in terms of safety process artifacts, and how such upstream artifacts can be automatically incorporated in an integration project's infrastructure for generation/collation/validation of safety artifacts.
The goal is to have a blueprint which can serve as actionable input for open source projects that want to become more "automotive grade", as well as for integration project infrastructure where reproducible builds, software compliance processes and safety engineering are key.

## The Doing

This is currently a small group of likeminded people occasionally talking, trying to come to grips with the overall problem statement and whether there is anything that can be usefully done about it.
If you are interested in participating, drop a note to daniel.krippner@etas.com, create an issue in this project or submit a PR.
