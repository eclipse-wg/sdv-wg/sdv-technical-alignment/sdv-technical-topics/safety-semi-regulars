# Examples of challenging (hard to address in practice) AoUs

Integration-level AoU:

## 1) The integrator of [AUTOMOTIVE OS&Application STACK] shall provide sufficient resources (RAM, ROM, stack, CPU runtime or operating system resources) for the stack.

In combination with OS AoU:

## 2) The system shall be designed in such a way that running out of memory does not occur.

__Challenge:__ The real resources consumed by the stack depend on the generated code. Even if you know that consumption precisely, there is usually no fixed upper limit of resource consumption provided by the OS and the binaries that comprise the Application stack, as some of them are dependent on the specific use case and configuration.

The integrator needs to do profiling for worst case resource consumptions not only on his SW design (where worst case conditions can be known) but would need to also know which stimulus causes worst case consumption on entire Application Stack plus OS (which is normally not known).

__Solution:__ If the execution element of the OS + Stack allows a fail-safe design, it is sufficient if the Application STACK as well as the OS itself includes dedicated functional measures guarding from resource denial. This AoUs is rather a QM robustness requirement but not a safety AoU.  

---

## 3) The OS integrator should ensure that applications are granted only those privileges necessary to implement their functionality

__Challenge:__

a) This AoU is formulated as a recommendation, while from a technical point of view it should rather be a hard restriction ("shall"). In POSIX systems any privileges on direct HW port IO (x86 PCI access space) can circumvent MMU and IOMMU. Imagine a QM process with root capabilities or with permissions to directly write HW registers, or to open and block files in a kernel proc/pid workspace, so the kernel cannot access them appropriately.

b) In POSIX systems there are lots of such abilities and permissions possible (manipulate IPC peer process memory, set or change user/group ID on which e.g. access control list are based to coordinate e.g. access on shared resources like shared mem for IPC is implemented, inheriting permissions maybe unintentionally, setting of HW configurations, etc). The interference path is very hard to figure out, as is deciding which permissions to entirely forbid, which to allow only at a certain point in time, and which potentially are problematic but can be reliably detected by other already existing safety mechanisms, etc.
